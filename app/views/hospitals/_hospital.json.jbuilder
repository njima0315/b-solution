json.extract! hospital, :id, :hospitalname, :prefe, :addr, :doctor, :type, :period, :body, :purpose, :need, :proposal, :result, :created_at, :updated_at
json.url hospital_url(hospital, format: :json)
