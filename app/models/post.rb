class Post < ApplicationRecord
  belongs_to :user
  scope :desc, -> { order("posts.created_at DESC") }
end
