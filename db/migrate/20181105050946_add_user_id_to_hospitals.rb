class AddUserIdToHospitals < ActiveRecord::Migration[5.2]
  def change
    add_column :hospitals, :user_id, :integer
  end
end
