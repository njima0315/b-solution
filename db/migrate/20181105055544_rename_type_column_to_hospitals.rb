class RenameTypeColumnToHospitals < ActiveRecord::Migration[5.2]
  def change
    rename_column :hospitals, :type, :kind
  end
end
