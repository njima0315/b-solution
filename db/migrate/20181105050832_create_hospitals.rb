class CreateHospitals < ActiveRecord::Migration[5.2]
  def change
    create_table :hospitals do |t|
      t.string :hospitalname
      t.string :prefe
      t.string :addr
      t.string :doctor
      t.string :type
      t.date :period
      t.text :body
      t.text :purpose
      t.text :need
      t.text :proposal
      t.text :result

      t.timestamps
    end
  end
end
