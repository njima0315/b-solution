Rails.application.routes.draw do

  resources :hospitals
  devise_for :users

  resources :posts

  get 'solutions/index'
  get 'solutions/show'

  root to: 'solutions#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
