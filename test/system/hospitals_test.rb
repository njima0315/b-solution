require "application_system_test_case"

class HospitalsTest < ApplicationSystemTestCase
  setup do
    @hospital = hospitals(:one)
  end

  test "visiting the index" do
    visit hospitals_url
    assert_selector "h1", text: "Hospitals"
  end

  test "creating a Hospital" do
    visit hospitals_url
    click_on "New Hospital"

    fill_in "Addr", with: @hospital.addr
    fill_in "Body", with: @hospital.body
    fill_in "Doctor", with: @hospital.doctor
    fill_in "Hospitalname", with: @hospital.hospitalname
    fill_in "Need", with: @hospital.need
    fill_in "Period", with: @hospital.period
    fill_in "Prefe", with: @hospital.prefe
    fill_in "Proposal", with: @hospital.proposal
    fill_in "Purpose", with: @hospital.purpose
    fill_in "Result", with: @hospital.result
    fill_in "Type", with: @hospital.type
    click_on "Create Hospital"

    assert_text "Hospital was successfully created"
    click_on "Back"
  end

  test "updating a Hospital" do
    visit hospitals_url
    click_on "Edit", match: :first

    fill_in "Addr", with: @hospital.addr
    fill_in "Body", with: @hospital.body
    fill_in "Doctor", with: @hospital.doctor
    fill_in "Hospitalname", with: @hospital.hospitalname
    fill_in "Need", with: @hospital.need
    fill_in "Period", with: @hospital.period
    fill_in "Prefe", with: @hospital.prefe
    fill_in "Proposal", with: @hospital.proposal
    fill_in "Purpose", with: @hospital.purpose
    fill_in "Result", with: @hospital.result
    fill_in "Type", with: @hospital.type
    click_on "Update Hospital"

    assert_text "Hospital was successfully updated"
    click_on "Back"
  end

  test "destroying a Hospital" do
    visit hospitals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hospital was successfully destroyed"
  end
end
